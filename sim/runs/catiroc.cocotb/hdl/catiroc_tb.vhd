-- * Libraries

library ieee;
use ieee.std_logic_1164.all;
use work.catiroc_pkg.all;
use work.ft2232h_instance_pkg.all;
use work.edacom_pkg.all;

library osvvm;
context osvvm.OsvvmContext;

-- * Entity

entity catiroc_tb is
  generic (
    g_nb_asics : natural range 1 to 8 := 1;
    g_osvvm_loglevel    : string               := "none"
    );
  port (
    -- usb if
    usb_rxf      : in  std_logic;
    usb_txe      : in  std_logic;
    usb_oe       : out std_logic;
    usb_rd       : out std_logic;
    usb_wr       : out std_logic;
    usb_data_in  : in  std_logic_vector(7 downto 0);
    usb_data_out : out std_logic_vector(7 downto 0);
    -- asic
    trig_ext     : out std_logic_vector(g_nb_asics - 1 downto 0)
    );
end entity catiroc_tb;

-- * Architecture

architecture test_tb of catiroc_tb is

  -- ** Signals

  signal catirocs : rc_catiroc_array(0 to g_nb_asics - 1);

  signal rst           : std_logic;
  signal usb_if        : rc_usb_if;
  signal global_reset  : std_logic;
  signal disable_40mhz : std_logic;

  signal p1_if : rc_peripheral_if_p1(params(0 to 0));
  signal p2_if : rc_peripheral_if_p2(params(0 to g_nb_asics - 1));
  signal p3_if : rc_peripheral_if_p3(params(0 to g_nb_asics - 1));
  signal p4_if : rc_peripheral_if_p4(params(0 to 0));
  signal p5_if : rc_peripheral_if_p5(params(0 to 0));
  signal p6_if : rc_peripheral_if_p6(params(0 to 0));

  signal discri_falling    : t_nbasics_2bytes(0 to g_nb_asics - 1);
  signal neweventbychannel : t_nbasics_2bytes(0 to g_nb_asics - 1);

  signal clk               : std_logic := '0';
  signal clk_2232          : std_logic := '0';
  signal clk_sc            : std_logic := '0';
  signal clk_80mhz         : std_logic := '0';
  signal clk_80mhz_delayed : std_logic := '0';
  signal clk_40mhz         : std_logic := '0';
  signal clk_120mhz        : std_logic := '0';
  signal clk_160mhz        : std_logic := '0';
  signal clk_480mhz        : std_logic := '0';

  signal osvvm_do_report   : boolean := false;  -- when to do the coverage report

begin

  -- ** Transcripts

  u_transcript : process
  begin
    -- SetTranscriptMirror;
    TranscriptOpen("./transcript.org");  -- Transcript file
    wait;
  end process u_transcript;

  -- ** Logging
  --
  -- Set log levels

  u_alert : process
  begin

    SetAlertLogName("AlertLog_Demo_Global");
    edacom_SetLogLevel(g_osvvm_loglevel);

    wait;

  end process u_alert;

  -- ** Reports

  -- Produce coverage and scoreboard reports

  u_reports : process
  begin

    -- wait until tb enables producing the report
    wait until osvvm_do_report;
    -- SetTranscriptMirror(false);
    BlankLine(1);

    -- Log("DATA Statistics:", ALWAYS);
    -- BlankLine(1);
    -- cp_dataout.writebin;

    -- Log("CHANNEL Statistics:", ALWAYS);
    -- BlankLine(1);
    -- cp_channel.writebin;

    -- BlankLine(1);

    -- Log("Scoreboard: channel/data errors: " & to_string(sb_channel_data.GetErrorCount) &
    --     "  Push counts: " & to_string(sb_channel_data.GetPushCount) &
    --     "  Check couts: " & to_string(sb_channel_data.GetCheckCount), ALWAYS);
    -- BlankLine(1);

    -- Log("Scoreboard: channel errors: " & to_string(sb_channel.GetErrorCount) &
    --     "  Push counts: " & to_string(sb_channel.GetPushCount) &
    --     "  Check couts: " & to_string(sb_channel.GetCheckCount), ALWAYS);
    -- BlankLine(1);

    -- Log("Scoreboard: data errors: " & to_string(sb_data.GetErrorCount) &
    --     "  Push counts: " & to_string(sb_data.GetPushCount) &
    --     "  Check couts: " & to_string(sb_data.GetCheckCount), ALWAYS);
    -- BlankLine(1);

    ReportAlerts;
    TranscriptClose;

  end process u_reports;

  -- ** Logic

  -- Logic to test

  -- *** Clock

  -- System clock

  CreateClock(clk => clk, Period => 8 ns);

  CreateClock(clk => clk_2232, Period => 12 ns);

  CreateClock(clk => clk_sc, Period => 100 ns);

  CreateClock(clk => clk_80mhz, Period => 12.5 ns);

  CreateClock(clk => clk_80mhz_delayed, Period => 12.5 ns);

  CreateClock(clk => clk_40mhz, Period => 25 ns);

  CreateClock(clk => clk_120mhz, Period => 8.3 ns);

  CreateClock(clk => clk_160mhz, Period => 6.25 ns);

  CreateClock(clk => clk_480mhz, Period => 20.8 ns);

  -- *** Reset

  -- System reset

  CreateReset(reset       => rst,
              resetactive => '1',
              clk         => clk,
              Period      => 100 ns,
              tpd         => 0 ns);

  -- *** Catiroc
  --
  -- **** force trigger
  --
  -- Force trigger signal sent by the catiroc controller

  u_trig_ext: for i in catirocs'range generate
    trig_ext(i) <= catirocs(i).trig_ext;
  end generate u_trig_ext;

  -- **** controller

  u_catiroc : entity work.catiroc
    port map (
      global_reset      => global_reset,
      -- asics if
      catirocs          => catirocs,
      -- peripheral if
      p2_if             => p2_if,
      p3_if             => p3_if,
      pll_locked        => '1',
      -- clocking
      clk_sc            => clk_sc,
      clk_80mhz         => clk_80mhz,
      clk_80mhz_delayed => clk_80mhz_delayed,
      clk_40mhz         => clk_40mhz,
      clk_120mhz        => clk_120mhz,
      clk_160mhz        => clk_160mhz,
      clk_480mhz        => clk_480mhz,
      -- outs
      discri_falling    => discri_falling,
      neweventbychannel => neweventbychannel
      );

  -- **** model

  u_catiroc_model : entity work.catiroc_model
    port map (
      catirocs => catirocs
      );

  -- *** Usb
  --
  -- **** signaling
  --
  -- Necessary as long as the cocotb/ghdl interface is unable to handles records. To be fixed one
  -- day.

  usb_if.clk_2232 <= clk_2232;

  usb_if.usb_rxf <= usb_rxf;

  usb_if.usb_txe <= usb_txe;

  usb_oe <= usb_if.usb_oe;

  usb_rd <= usb_if.usb_rd;

  usb_wr <= usb_if.usb_wr;

  usb_data_out <= usb_if.usb_data;

  usb_if.usb_data <= usb_data_in;

  -- **** controller

  u_ft2232h : entity work.ft2232h
    port map (
      main_rst      => rst,
      usb_if        => usb_if,
      p1_if         => p1_if,
      p2_if         => p2_if,
      p3_if         => p3_if,
      p4_if         => p4_if,
      p5_if         => p5_if,
      p6_if         => p6_if,
      disable_40mhz => disable_40mhz,
      global_reset  => global_reset
      );

  -- *** Counter

  u_p1_counter : entity work.p1_counter
    port map (
      clk          => clk,
      global_reset => global_reset,
      p_if         => p1_if
      );

end architecture test_tb;

-- * Configuration

configuration catiroc_tb_test_tb_cfg of catiroc_tb is
  for test_tb
  end for;
end catiroc_tb_test_tb_cfg;
