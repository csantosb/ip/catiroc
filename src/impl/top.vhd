library ieee;
use ieee.std_logic_1164.all;
use work.catiroc_pkg.all;
use work.ft2232h_instance_pkg.all;

entity top is
  generic (nb_asics : natural range 1 to 8 := 8);
  port (global_reset       : in    std_logic;
        catirocs           : inout rc_catiroc_array(0 to nb_asics-1);  --! all asics
        p2_if              : inout rc_peripheral_if_p2(params(0 to nb_asics-1));  --! p2: asic data
        p3_if              : inout rc_peripheral_if_p3(params(0 to nb_asics-1));  --! p3: asic slow control
        -- clocking
        clk_sc             : in    std_logic;
        clk_80mhz          : in    std_logic;
        clk_80mhz_delayed  : in    std_logic;
        clk_40mhz          : in    std_logic;
        clk_120mhz         : in    std_logic;
        clk_160MHz         : in    std_logic;
        clk_480mhz         : in    std_logic;
        -- outs
        discri_falling     : out   t_nbasics_2bytes;
        NewEventByChannel  : out   t_nbasics_2bytes);
end entity top;

architecture simple of top is

begin

  u_catiroc : entity work.catiroc
    port map (global_reset       => global_reset,
              -- asics if
              catirocs           => catirocs,
              -- peripheral if
              p2_if              => p2_if,
              p3_if              => p3_if,
              pll_locked         => '1',
              -- clocking
              clk_sc             => clk_sc,
              clk_80mhz          => clk_80mhz,
              clk_80mhz_delayed  => clk_80mhz_delayed,
              clk_40mhz          => clk_40mhz,
              clk_120mhz         => clk_120mhz,
              clk_160MHz         => clk_160MHz,
              clk_480mhz         => clk_480mhz,
              -- outs
              discri_falling     => discri_falling,
              NewEventByChannel  => NewEventByChannel);

end architecture simple;
