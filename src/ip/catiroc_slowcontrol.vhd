-- * Catiroc Slowcontrol

--! @file
--! @brief Example peripheral implementing a simple up counter
--!
--! This example peripheral gives an example of use of a simple counter
--! It initializes four
--! ble, ble ...
--! bly, bly ...

-- * Libraries

library ieee;

-- * Packages

use ieee.std_logic_1164.all;
use work.catiroc_pkg.all;
use work.ft2232h_instance_pkg.all;
use work.edacom_pkg.all;

-- * Entity

entity catiroc_slowcontrol is
  port (
    global_reset : in    std_logic;
    catirocs     : inout rc_catiroc_array;     -- all asics
    p_if         : inout rc_peripheral_if_p3;  -- peripheral interface
    -- clocking
    clk_sc       : in    std_logic;            -- slow control clock
    -- out
    resetb       : out   std_logic;
    start_system : out   std_logic
    );
end entity catiroc_slowcontrol;

-- * Architecture

architecture behavioral of catiroc_slowcontrol is

  -- ** Signals

  signal fifo_rd_if : t_fifo_rd_if_array(p_if.params'range)(data(p3_data_width * 8 - 1 downto 0));

begin

  -- ** nb_asics slow control instances

  u_catiroc_sc : for i in p_if.params'range generate
    u_catiroc_sc_1ch : entity work.catiroc_slowcontrol_1ch
      port map (
        clk_sc       => clk_sc,
        global_reset => global_reset,
        resetb       => resetb,
        start_system => start_system,
        catiroc      => catirocs(i),
        enable       => p_if.enable,
        params       => p_if.params(i),
        fifo_rd_if   => fifo_rd_if(i)
        );

  end generate u_catiroc_sc;

  -- ** multiplexor nb_asics to 1

  -- Interfaces the output of all catiroc managers to the usb interface. This is
  -- necessary to act as a multiplexer from “nb_asics” to one. This means that this
  -- modules reads data from “nb_asics”, “input_bus_width_bits”-bits data fifos and
  -- tramsfers it to a single “input_bus_width_bits”-bits data fifo to be accessed
  -- from the [[*USB Interface][USB Interface]].

  -- This instance transfers the slow control registers read from the “sc_out” of
  -- the ASICS. P3 command.

  -- The multiplexor, to send data from =nb_asics= modules to the [[*USB Interface][USB Interface]]. I do
  -- use =extend= here, so the data i/o data bus width is not respected: at the output,
  -- we bet one more byte including the asic number from where the data originates.

  U_mux_fifo_slowcontrol : entity work.mux_fifo_wrapper_sc
    generic map (
      g_extend => 1
      )
    port map (
      global_reset => global_reset,
      fifo_rd_if   => fifo_rd_if,
      p_if         => p_if
      );

end architecture behavioral;
