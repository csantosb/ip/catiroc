-- * Catiroc

--! @file
--! @brief Example peripheral implementing a simple up counter
--!
--! This example peripheral gives an example of use of a simple counter
--! It initializes four
--! ble, ble ...
--! bly, bly ...

-- * Libraries

library IEEE;

-- * Packages
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.catiroc_pkg.all;
use work.ft2232h_instance_pkg.all;

-- * Entity

--! Mux entity brief description

--! Detailed description of this
--! mux design element.

entity catiroc is
  port (
    global_reset      : in    std_logic;
    catirocs          : inout rc_catiroc_array;     --! all asics
    p2_if             : inout rc_peripheral_if_p2;  --! p2: asic data
    p3_if             : inout rc_peripheral_if_p3;  --! p3: asic slow control
    pll_locked        : in    std_logic;
    -- clocking
    clk_sc            : in    std_logic;
    clk_80mhz         : in    std_logic;
    clk_80mhz_delayed : in    std_logic;
    clk_40mhz         : in    std_logic;
    clk_160mhz        : in    std_logic;
    clk_480mhz        : in    std_logic;
    clk_120mhz        : in    std_logic;
    -- outs
    discri_falling    : out   t_nbasics_2bytes;
    neweventbychannel : out   t_nbasics_2bytes
    );
end entity catiroc;

-- * Architecture

--! @brief Architecture definition of the MUX
--! @details More details about this mux element.

architecture behavioral of catiroc is

  -- ** Signals

  signal resetb_capture : std_logic;
  signal resetb_config  : std_logic;

  --! Detailed description of this
  -- signal sr_rst_sc : std_logic := '1';

begin

  -- ** P3 - Slow Control, "CC" Command
  --
  --! Slow Control Management.
  --!
  --! Implements sending control registers to asic, and reading them back.
  --! When the software sends a command to peripheral 3, the parameters are
  --! transferred to the asic twice. As slow control is sent through a shift register,
  --! its output is read back here and sent to the usb. The software will read usb
  --! contents, and compare with data sent.

  u_catiroc_slowcontrol : entity work.catiroc_slowcontrol
    port map (
      global_reset => global_reset,
      catirocs     => catirocs,
      p_if         => p3_if,
      -- clocking
      clk_sc       => clk_sc,
      -- outs
      resetb       => resetb_config,
      start_system => open
      );

  -- ** P2 - Data Capture, "06" Command

  -- Data Capture Management.

  -- Implements ...

  -- u_handle_dataout_top : entity work.catiroc_capture
  --   port map (
  --     global_reset      => global_reset,
  --     catirocs          => catirocs,
  --     p_if              => p2_if,
  --     pll_locked        => pll_locked,
  --     -- clocking
  --     clk_80mhz         => clk_80mhz,
  --     clk_80mhz_delayed => clk_80mhz_delayed,
  --     clk_40mhz         => clk_40mhz,
  --     clk_480mhz        => clk_480mhz,
  --     clk_120mhz        => clk_120mhz,
  --     clk_160mhz        => clk_160MHz,
  --     -- outs
  --     neweventbychannel => NewEventByChannel,
  --     discri_falling    => discri_falling,
  --     resetb            => resetb_capture
  --     );

  -- ** Combine signals

  -- Common =restb= (resets the ASIC) for the two blocks.

  -- resetb <= resetb_capture and resetb_config;
  -- catiroc.resetb <= resetb_capture;

  -- Common =sr_rst= (resets the SC shift register, active low) for the sc
  --    and the =main_rst=.

  -- reset cati before sending configuration
  -- sr_rst <= not (main_rst) and sr_rst_sc(delq 'company-capf company-backends)

  -- catiroc.sr_rst <= not (main_rst);

end architecture behavioral;
