-- * catiroc_emulator

--! @file
--! @brief Example peripheral implementing a simple up counter
--!
--! This example peripheral gives an example of use of a simple counter
--! It initializes four
--! ble, ble ...
--! bly, bly ...

-- Module to simulate data output of catiROC ASIC from Omega.

--    Here, we output a serial data stream 'dataout' and 'strobe', synchronous to
--    'clk_out'. Following the status of 'emulated', real data taken from
--    'cati_dataout' and 'cati_strobe' is output.
--                                           A global init reset 'main_rst' brings things to a known status; 'enabled'
--                                                                                                              activates the module, synchronous to 'clk_in'.

--                                                                                                              The module emulates 'nb_channels' between 1 and 8; 'active_channels' selects
--                                                                                                                                                                    the channels active, for example, 'active_channels="83"' and
--                                                                                                                                                                    'nb_channels=3' will generate data for channels 1, 2 and 8. These generics
--                                                                                                                                                                                                     must be coherent.

-- * Libraries

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.numeric_std.all;

-- * Packages

-- * Entity

entity catiroc_emulator is
  generic (nb_channels     : natural range 1 to 8 := 2;  -- nb of active channels
           active_channels : std_logic_vector(7 downto 0));  -- active channels
  port (clk_in       : in  std_logic;   -- clk in @ X MHz
        rst          : in  std_logic;   -- init reset
        emulated     : in  std_logic;  -- select between real and emulated data
        --
        -- asic data
        --
        cati_dataout : in  std_logic;   -- data from asic
        cati_strobe  : in  std_logic;   -- strobe from asic
        --
        -- emulated data
        --
        clk_out      : out std_logic;   -- clk out @ clk_in frequency
        dataout      : out std_logic;   -- serial data out
        strobe       : out std_logic);  -- data valid, active low
end entity catiroc_emulator;

-- * Architecture

architecture Behavioral of catiroc_emulator is

-- ** Signals

  -- fix value; space in between two successive strobes
  constant delay : natural range 0 to 63 := 10;

-- ucounter_main will +1 increase endlessly; its length is given by the
-- nb_channels generic and equal to 7 bits (0 to 127) for one channel, 8
-- bits (0 to 255) for two channels, etc.
  signal ucounter_main : unsigned(6+(nb_channels-1) downto 0) := (others => '0');

-- internal shift register; updated at each ucounter_main cycle; its leftmost
-- value is to be sent out to 'dataout'; its length is 29+21 cycles times nb_channels
  signal dataout_shifter       : std_logic_vector(50*nb_channels-1 downto 0) := (others => '0');
  signal dataout_shifter_inner : std_logic_vector(50*nb_channels-1 downto 0) := (others => '0');

-- internal signals
  signal dataout_inner : std_logic := '0';
  signal strobe_inner  : std_logic := '0';
  signal toto          : std_logic := '0';

-- Following "Digital Readout CatiROC.docx"
  signal coarse_time : unsigned(25 downto 0) := (others => '0');
  signal ADC_charge  : unsigned(9 downto 0)  := (others => '0');
  signal ADC_time    : unsigned(9 downto 0)  := (others => '0');
-- signal Gain        : std_logic             := '1';

begin

  -- ** Generic

  clk_out <= not(clk_in);

  -- ** Main proces

  U0 : process (clk_in, rst) is

    variable j : natural range 0 to 8 := 0;

  begin

    if rst = '1' then

      ucounter_main         <= (others => '0');
      coarse_time           <= (others => '0');
      ADC_charge            <= (others => '0');
      ADC_time              <= (others => '0');
      strobe_inner          <= '1';
      dataout_shifter       <= (others => '0');
      dataout_shifter_inner <= (others => '0');
      dataout_inner         <= '0';

    elsif clk_in'event and clk_in = '1' then

      -- ** Main cycle

      -- loop over 128 times nbchannels clock counts
      ucounter_main <= ucounter_main + 1;  -- will reset after reaching its maximum

      -- ** Update data

      -- every cycle, increase coarse time, charge and time registers
      if ucounter_main = 0 then
        coarse_time <= coarse_time + 1;
        ADC_charge  <= ADC_charge + 2;
        ADC_time    <= ADC_time + 3;
      end if;

-- fill in the dataout_shifter register with meaningfull data
-- use one clock cycle by active channel

--  dataout_shifter  <= (others => '0');  -- reset register

-- j := 0;
-- for i in 1 to 8 loop
--    if ucounter_main = i and active_channels(i-1) = '1' then
--       j := j + 1;
--      test_i <= std_logic_vector(to_unsigned(i, 4)) ;
-- test_j <= std_logic_vector(to_unsigned(j, 4)) ;
-- test with: nb_channels=2; i=1; 50*nb_channels-1-29*(i-1), 50*nb_channels-1-29*i+1
--       dataout_shifter_inner(dataout_shifter_inner'left-29*(j-1) downto dataout_shifter_inner'left-29*j+1)
--          <= std_logic_vector(to_unsigned(i-1, 3)) & std_logic_vector(coarse_time);
-- test with: nb_channels=2; i=2; 50*nb_channels-1-29*nb_channels-21*(i-1), 50*nb_channels-29*nb_channels-1-21*i+1
-- --        dataout_shifter_inner(dataout_shifter_inner'left-29*nb_channels-21*(j-1) downto dataout_shifter_inner'left-29*nb_channels-21*j+1)
--           <= (others=>'1'); --Gain & std_logic_vector(ADC_charge) & std_logic_vector(ADC_time);
--     end if;
--  end loop;


      if ucounter_main = 5 then
        for j in 1 to nb_channels loop
          -- test_i <= std_logic_vector(to_unsigned(i, 4)) ;
          -- test_j <= std_logic_vector(to_unsigned(j, 4)) ;
          -- test with: nb_channels=2; i=1; 50*nb_channels-1-29*(i-1), 50*nb_channels-1-29*i+1
          dataout_shifter_inner(dataout_shifter_inner'left-29*(j-1) downto dataout_shifter_inner'left-29*j+1)
            <= std_logic_vector(to_unsigned(j-1, 3)) & std_logic_vector(coarse_time);
          -- test with: nb_channels=2; i=2; 50*nb_channels-1-29*nb_channels-21*(i-1), 50*nb_channels-29*nb_channels-1-21*i+1
          dataout_shifter_inner(dataout_shifter_inner'left-29*nb_channels-21*(j-1) downto dataout_shifter_inner'left-29*nb_channels-21*j+1)
            <= '1' & std_logic_vector(ADC_charge) & std_logic_vector(ADC_time);
        end loop;
      end if;

      -- ** Strobe cycle

      if (ucounter_main = delay*nb_channels) then
        dataout_shifter <= dataout_shifter_inner;
      elsif (ucounter_main > delay*nb_channels
             and ucounter_main <= (delay*nb_channels + 29*nb_channels)) then
        strobe_inner    <= '0';
        -- shifted left while strobe is low
        dataout_shifter <= dataout_shifter(dataout_shifter'left-1 downto 0) & '0';
      elsif ((ucounter_main > (delay*nb_channels + 29*nb_channels + delay*nb_channels))
             and (ucounter_main <= (delay*nb_channels + 29*nb_channels +
                                    delay*nb_channels + 21*nb_channels))) then
        strobe_inner    <= '0';
        -- shifted left while strobe is low
        dataout_shifter <= dataout_shifter(dataout_shifter'left-1 downto 0) & '0';
      else
        strobe_inner <= '1';
      end if;

      -- ** Data output

      dataout_inner <= dataout_shifter(dataout_shifter'left);

    end if;

  end process U0;

  -- ** Multiplex process

  U1 : process (clk_in, rst) is
  begin
    if rst = '1' then
      dataout <= '0';
      strobe  <= '0';
    elsif clk_in'event and clk_in = '1' then
      if emulated = '1' then
        dataout <= dataout_inner;
        strobe  <= strobe_inner;
      else
        dataout <= cati_dataout;
        strobe  <= cati_strobe;
      end if;
    end if;
  end process U1;

end architecture Behavioral;
