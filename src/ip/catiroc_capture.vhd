-- * Catiroc_capture

--! @file
--! @brief Example peripheral implementing a simple up counter
--!
--! This example peripheral gives an example of use of a simple counter
--! It initializes four
--! ble, ble ...
--! bly, bly ...

-- * Libraries

library ieee;

-- * Packages

use ieee.std_logic_1164.all;
use work.catiroc_pkg.all;
use work.ft2232h_instance_pkg.all;
use work.edacom_pkg.all;

-- * Entity

entity catiroc_capture is
  port(global_reset      : in    std_logic;
       catirocs          : inout rc_catiroc_array;     -- all asics
       p_if              : inout rc_peripheral_if_p2;  -- peripheral interface
       pll_locked        : in    std_logic;
       -- clocking
       clk_80Mhz         : in    std_logic;
       clk_80Mhz_delayed : in    std_logic;
       clk_40Mhz         : in    std_logic;
       clk_480MHz        : in    std_logic;
       clk_120MHz        : in    std_logic;
       clk_160MHz        : in    std_logic;
       -- outs
       NewEventByChannel : out   t_nbasics_2bytes;
       discri_falling    : out   t_nbasics_2bytes;
       resetb            : out   std_logic);
end entity catiroc_capture;

-- * Architecture

architecture Behavioral of catiroc_capture is

  -- ** Signals

  signal fifo_rd_if : t_fifo_rd_if_array(p_if.params'range)(data(p2_data_width * 8 - 1 downto 0));

  -- signal fifo_if : t_fifo_if(data(0 to nb_asics-1)(p2_data_width*8-1 downto 0),
  --                            empty(0 to nb_asics-1),
  --                            rd(0 to nb_asics-1));

begin

  -- ** nb_asics data capture instances

  U_catiroc_capture : for i in p_if.params'range generate
    u_catiroc_capture_1ch : entity work.catiroc_capture_1ch
      port map (catiroc           => catirocs(i),
                global_reset      => global_reset,
                NewEvent          => open,
                NewEventByChannel => NewEventByChannel(i),
                pll_locked        => pll_locked,
                discri_falling    => discri_falling(i),
                resetb            => resetb,
                enable            => p_if.enable,
                params            => p_if.params(i)(7 downto 0),
                --
                clk_80Mhz         => clk_80Mhz,
                clk_80Mhz_delayed => clk_80Mhz_delayed,
                clk_40Mhz         => clk_40Mhz,
                clk_160MHz        => clk_160MHz,
                clk_480MHz        => clk_480MHz,
                clk_120MHz        => clk_120MHz,
                --
                fifo_rd_if   => fifo_rd_if(i)
                );
  end generate;

  -- ** multiplexor nb_asics to 1

  -- Interfaces the output of all catiroc modules to the usb interface. This is
  -- necessary to act as a multiplexer from “nb_asics” to one. This means that this
  -- modules reads data from “nb_asics”, “input_bus_width_bits”-bits data fifos and
  -- transfers it to a single “input_bus_width_bits”-bits data fifo to be accessed
  -- from the [[*USB Interface][USB Interface]]..

  -- This instance transfers the data read from the ASICS. P2 command.

  -- The multiplexor, to send data from =nb_asics= modules to the [[*USB Interface][USB Interface]]. The
  -- input data bus width (in bytes) is given by =p3_data_width=. The output data bus
  -- width will be the same, or =p3_data_width+2=, following the value of the =extend=
  -- generic. This is use to extend the width of the data bus, to accommodate the
  -- board identifier (to distinguish cards), and the asic number. This is done
  -- within the module itself.

  U_mux_fifo_capture : entity work.mux_fifo_wrapper_capture
    generic map (
      g_extend => 1
      )
    port map (
      global_reset => global_reset,
      fifo_rd_if   => fifo_rd_if,
      p_if         => p_if
      );

end architecture Behavioral;
