-- * catiroc_slowcontrol_1ch

--! @file
--! @brief Example peripheral implementing a simple up counter
--!
--! This example peripheral gives an example of use of a simple counter
--! It initializes four
--! ble, ble ...
--! bly, bly ...

-- * Libraries

library IEEE;

library UNISIM;

library osvvm;
context osvvm.OsvvmContext;

-- * Packages

use IEEE.STD_LOGIC_1164.all;
use IEEE.numeric_std.all;
use UNISIM.VComponents.all;
use work.catiroc_pkg.all;
use work.ft2232h_instance_pkg.all;
use work.edacom_pkg.all;

-- * Entity

entity catiroc_slowcontrol_1ch is
  port (
    clk_sc       : in    std_logic;     -- generic oscilator present on board
    global_reset : in    std_logic;
    catiroc      : inout rc_catiroc;    -- cati
    resetb       : out   std_logic;
    start_system : out   std_logic;
    enable       : in    std_logic;
    params       : in    std_logic_vector(p3_params_width * 8 - 1 downto 0);
    --
    fifo_rd_if   : inout t_fifo_rd_if
    );
end entity catiroc_slowcontrol_1ch;

-- * Architecture

architecture behavioral of catiroc_slowcontrol_1ch is

  -- ** Types

  type slowcontrol_write_state is (
    s_standby, s_resetb, s_rst, s_send_sc, s_temp_1, s_temp_2, s_send_probe, s_start_system
    );

  -- ** Signals

  signal fifo_data_in        : std_logic_vector(7 downto 0)   := (others => '0');
  signal u_reset_counter     : unsigned (3 downto 0)          := (others => '0');
  signal u_sc_counter        : unsigned (11 downto 0)         := (others => '0');
  signal u_readback_counter  : unsigned(3 downto 0)           := (others => '0');
  signal u_proberead_counter : unsigned (7 downto 0)          := (others => '0');
  signal slow_control_reg    : std_logic_vector(327 downto 0) := (others => '0');
  signal probe_reg           : std_logic_vector(193 downto 0) := (others => '0');
  signal enable_readback_sc  : std_logic                      := '0';
  signal enable_readback_pb  : std_logic                      := '0';
  signal s_state             : slowcontrol_write_state;
  signal fifo_wr             : std_logic                      := '0';
  signal active_srclk        : std_logic                      := '0';
  signal not_active_srclk    : std_logic                      := '1';
  signal fifo_clear          : std_logic                      := '1';
  signal gnd_1bit            : std_logic                      := '0';
  signal vcc_1bit            : std_logic                      := '1';
  signal u_byte_count        : unsigned(10 downto 0)          := (others => '0');
  signal u_byte              : unsigned(7 downto 0)           := X"00";

  constant catisc_1ch_id          : alertlogidtype := GetAlertLogID("catisc_1ch_id");
  constant catisc_1ch_toasic_id   : alertlogidtype := GetAlertLogID("* catisc_1ch_toasic_id *", catisc_1ch_id);
  constant catisc_1ch_fromasic_id : alertlogidtype := GetAlertLogID("* catisc_1ch_fromasic_id *", catisc_1ch_id);

begin

  -- ** SR to ASIC

  -- Sends parameters as slow control registers to ASIC

  u_sc_send : process (clk_sc) is
  begin

    if (clk_sc'event and clk_sc = '1') then
      -- *** Reset

      if (global_reset = '1' or enable = '0') then
        resetb               <= '1';
        catiroc.sc.sr_in     <= '0';
        catiroc.sc.sr_rst    <= '1';
        catiroc.sc.sr_select <= '0';
        u_reset_counter      <= (others => '0');
        slow_control_reg     <= (others => '0');
        probe_reg            <= (others => '0');
        u_sc_counter         <= (others => '0');
        enable_readback_sc   <= '0';
        enable_readback_pb   <= '0';
        start_system         <= '1';
        active_srclk         <= '0';
        u_byte_count         <= (others => '0');
        u_byte               <= X"00";
        s_state              <= s_standby;
      else

        case s_state is

          -- *** Stand by

          when s_standby =>

            -- stand by state

            -- logging
            Log(catisc_1ch_toasic_id, "stand by ...", DEBUG);
            BlankLine(1);
            -- logging
            start_system         <= '1';
            catiroc.sc.sr_in     <= '0';
            u_reset_counter      <= (others => '0');
            u_sc_counter         <= (others => '0');
            enable_readback_sc   <= '0';
            enable_readback_pb   <= '0';
            active_srclk         <= '0';
            slow_control_reg     <= params(527 downto 200);
            probe_reg            <= params(199 downto 6);
            resetb               <= '1';
            catiroc.sc.sr_rst    <= '1';
            catiroc.sc.sr_select <= '1';
            u_byte_count         <= (others => '0');
            u_byte               <= X"00";
            s_state              <= s_resetb;

            -- *** Resetb

          when s_resetb =>

            -- produces a reset asserting resetb low during 15 clock cycles

            if (u_reset_counter = X"F") then
              -- logging
              Log(catisc_1ch_toasic_id, "done resetb.", DEBUG);
              BlankLine(1);
              -- logging
              resetb          <= '1';
              u_reset_counter <= (others => '0');
              s_state         <= s_rst;
            else
              -- logging
              Log(catisc_1ch_toasic_id,
                  "doing resetb, cycle ..." & to_string(u_reset_counter), DEBUG);
              BlankLine(1);
              -- logging
              u_reset_counter <= u_reset_counter + 1;
              resetb          <= '0';
              s_state         <= s_resetb;
            end if;

            -- *** Rst

          when s_rst =>

            -- produces a reset to the asic slow control asserting
            -- catiroc.sc.sr_rst low during 15 clock cycles

            if (u_reset_counter = X"F") then
              -- logging
              Log(catisc_1ch_toasic_id, "done sr_rst.", DEBUG);
              BlankLine(1);
              -- logging
              catiroc.sc.sr_rst <= '1';
              u_reset_counter   <= (others => '0');
              s_state           <= s_send_sc;
            else
              -- logging
              Log(catisc_1ch_toasic_id,
                  "doing sr_rst, cycle ..." & to_string(u_reset_counter), DEBUG);
              BlankLine(1);
              -- logging
              u_reset_counter   <= u_reset_counter + 1;
              catiroc.sc.sr_rst <= '0';
              s_state           <= s_rst;
            end if;

            -- *** Send slow control

          when s_send_sc =>

            -- send slow control register twice, meaning 656 bits (328x2 bits)
            -- sc.sr_select is high
            -- (2x SlControl + 8bit) --
            -- ca marche avec 664 (656 + 8bit)

            if (u_sc_counter < 656) then
              u_byte           <= u_byte(6 downto 0) & slow_control_reg(327);
              u_byte_count     <= u_byte_count + 1;
              active_srclk     <= '1';
              catiroc.sc.sr_in <= slow_control_reg(327);
              slow_control_reg <= slow_control_reg(326 downto 0) & slow_control_reg(327);
              u_sc_counter     <= u_sc_counter + 1;
              -- logging
              Log(catisc_1ch_toasic_id, "Sending sc bit n. " &
                  to_string(to_integer(u_sc_counter)) &
                  ", bit " &
                  to_string(slow_control_reg(327)), DEBUG);
              BlankLine(1);
              if (u_byte_count(2 downto 0) = "000") then
                Log(catisc_1ch_toasic_id,
                    "Sending sc byte n. " & to_string(to_integer(u_byte_count(10 downto 3))) &
                    ", byte " & to_hstring(u_byte), DEBUG);
                BlankLine(1);
              end if;
              -- logging
              -- when half of the bits have been sent, 328, allow reading back the incoming bits
              if (u_sc_counter < 328) then
                enable_readback_sc <= '0';
              else
                enable_readback_sc <= '1';
              end if;
              s_state <= s_send_sc;
            else
              active_srclk <= '0';
              u_sc_counter <= (others => '0');
              s_state      <= s_temp_1;
            end if;

            -- *** Temp 1

          when s_temp_1 =>

            -- tmp state 1, necessary ?

            -- logging
            Log(catisc_1ch_toasic_id, "Sending sc done. Proceeding to probe register.", DEBUG);
            BlankLine(1);
            -- logging
            enable_readback_sc   <= '0';
            catiroc.sc.sr_select <= '0';
            s_state              <= s_temp_2;

            -- *** Temp 2

          when s_temp_2 =>

            -- tmp state 2, necessary ?

            s_state <= s_send_probe;

            -- *** Send probe register

          when s_send_probe =>

            -- send probe register, sc.sr_select is low

            -- 388 194x2 (2x ProbeReg)
            if (u_sc_counter < 388) then
              -- logging
              Log(catisc_1ch_toasic_id,
                  "Sending probe bit n. " & to_string(to_integer(u_sc_counter)) & " with value " &
                  to_string(probe_reg(193)), DEBUG);
              BlankLine(1);
              -- logging
              active_srclk     <= '1';
              catiroc.sc.sr_in <= probe_reg(193);
              probe_reg        <= probe_reg(192 downto 0) & probe_reg(193);
              u_sc_counter     <= u_sc_counter + 1;
              -- start_readback_PB <= '1';
              -- 1er bit de relecture on a remplis les 194 premiers
              if (u_sc_counter < 194) then
                enable_readback_pb <= '0';
                s_state            <= s_send_probe;
              else
                enable_readback_pb <= '1';
                s_state            <= s_send_probe;
              end if;
            else
              active_srclk <= '0';
              u_sc_counter <= (others => '0');
              s_state      <= s_start_system;
            end if;

          -- *** Start system

          when s_start_system =>

            enable_readback_pb <= '0';
            start_system       <= '1';
            s_state            <= s_start_system;

        end case;

      end if;
    end if;

  end process u_sc_send;

  -- ** SR Readout

  -- Readout of slow control registers back to usb

  process (clk_sc) is
  begin

    if (clk_sc'event and clk_sc = '1') then
      if (global_reset = '1' or enable = '0') then
        fifo_clear          <= '1';
        fifo_wr             <= '0';
        fifo_data_in        <= (others => '0');
        u_readback_counter  <= (others => '0');
        -- clear fifo contents
        u_proberead_counter <= (others => '0');
      else
        -- release fifo reset
        fifo_clear <= '0';

        if (fifo_wr = '1') then
          -- logging
          Log(catisc_1ch_fromasic_id, "Capturing sc byte " & to_hstring(fifo_data_in), DEBUG);
          BlankLine(1);
        -- logging
        end if;

        -- readback slow control
        if (enable_readback_sc = '1') then
          -- logging
          Log(catisc_1ch_fromasic_id,
              "Capturing sc BIT " & to_string(catiroc.sc.sr_out) & " fifo_data_in is " & to_hstring(fifo_data_in)
, DEBUG);
          BlankLine(1);
          -- logging
          fifo_data_in <= fifo_data_in(6 downto 0) & catiroc.sc.sr_out;
          if (u_readback_counter = X"7") then
            fifo_wr            <= '1';
            u_readback_counter <= (others => '0');
          else
            fifo_wr            <= '0';
            u_readback_counter <= u_readback_counter + 1;
          end if;

        -- readback probe register
        elsif (enable_readback_pb = '1') then
          if (u_proberead_counter < 192) then
            fifo_data_in        <= fifo_data_in(6 downto 0) & catiroc.sc.sr_out;
            u_proberead_counter <= u_proberead_counter + 1;
            if (u_readback_counter = X"7") then
              fifo_wr            <= '1';
              u_readback_counter <= (others => '0');
            else
              fifo_wr            <= '0';
              u_readback_counter <= u_readback_counter + 1;
            end if;
          else
            if (u_readback_counter = X"1") then
              fifo_data_in       <= fifo_data_in(0) & catiroc.sc.sr_out & B"000000";
              fifo_wr            <= '1';
              u_readback_counter <= (others => '0');
            else
              fifo_data_in       <= fifo_data_in(6 downto 0) & catiroc.sc.sr_out;
              fifo_wr            <= '0';
              u_readback_counter <= u_readback_counter + 1;
            end if;
          end if;
        else
          fifo_data_in        <= (others => '0');
          fifo_wr             <= '0';
          u_readback_counter  <= (others => '0');
          u_proberead_counter <= (others => '0');
        end if;
      end if;
    end if;

  end process;

  -- ** Slow control clock

  u_oddr : oddr
    generic map (
      ddr_clk_edge => "OPPOSITE_EDGE",
      init         => '0',
      srtype       => "SYNC"
      )
    port map (
      q  => catiroc.sc.sr_clk,
      c  => clk_sc,
      ce => vcc_1bit,
      d1 => vcc_1bit,
      d2 => gnd_1bit,
      r  => not_active_srclk,
      s  => gnd_1bit
      );

  not_active_srclk <= not(active_srclk);

  -- ** Fifo

  u_fifo : entity work.cati_output_fifosc
    port map (
      rst    => fifo_clear,
      --
      wr_clk => clk_sc,
      din    => fifo_data_in,
      wr_en  => fifo_wr,
      full   => open,
      --
      rd_clk => fifo_rd_if.clk,
      rd_en  => fifo_rd_if.rd,
      dout   => fifo_rd_if.data,
      empty  => fifo_rd_if.empty
      );

end architecture behavioral;
