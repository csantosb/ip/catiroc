#!/bin/sh

# create gtags.files with al vhd files in project
find ./ -type f \( -iname \*.vhd -o -iname \*.vhdl \) -print > gtags.files

/usr/bin/gtags

echo "Done."
